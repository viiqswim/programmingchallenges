The problem was the following
  Implement a system in PHP that is able to return a compressed
  string from a lowercase only characters sequence, only if the
  resulting compressed string is smaller. The system should also
  be able to decompress the compressed string.
  - compress "aaaabbcc" => "a4b2c2"
  - compress "aabb" => "aabb" (a2b2 is not smaller than aabb)
  - decompress "a4b2c2" => "aaaabbcc"

I implemented this with PHP and wrote Unit Tests,
applying a test-driven development with PHPUnit.