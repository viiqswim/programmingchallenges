<?php
Class FileArchiver {

    public function compress($string) {
        $compressed_string = $this->compressString($string);

        // If both strings (decompressed and compressed)
        // are the same length, that means that it doesn't
        // matter if we compress it, so just return the same
        // thing
        if ($this->isSameOrGreaterLength($string, $compressed_string)) {
            return $string;
        }

        return $compressed_string;
    }

    public function decompress($string) {
        if(strlen($string) == 1){
            return "error";
        }
        $decompressed_string = '';

        // For each letter in the compressed string
        // check the number that is right in front of the letter
        // and then ouput the letter we are checking that many times
        for ($i = 0; $i < strlen($string); $i = $i + 2) {
            $letter = $string[$i];
            for ($j = 0; $j < $string[$i + 1]; $j++) {
                $decompressed_string .= $letter;
            }
        }

        return $decompressed_string;
    }

    private function compressString($string) {
        $compressed_string = '';
        $last_letter = '';
        $count = 0;

        // For each letter in the string 
        // count the number of times each letter appears.
        // When the letter changes, then we record the number
        for ($i = 0; $i < strlen($string); $i++) {
            if (empty($last_letter)) {
                $count++;
            } else if ($last_letter == $string[$i]) {
                $count++;
                continue;
            } else {
                $compressed_string .= $last_letter . $count;
                $count = 1;
            }
            $last_letter = $string[$i];
        }
        $compressed_string .= $last_letter . $count;

        return $compressed_string;
    }

    private function isSameOrGreaterLength($str1, $str2) {
        if (strlen($str1) <= strlen($str2)) {
            return true;
        }
        return false;
    }

}

// Main control function
function main() {
    $fileArchiver = new FileArchiver();
    
    echo "aaaabbcc -> " . $fileArchiver->compress("aaaabbcc") . "<br />";
    echo "aaaa -> " . $fileArchiver->compress("aaaa") . "<br />";
    echo "aabb -> " . $fileArchiver->compress("aabb") . "<br />";
    echo "a4b2c2 -> " . $fileArchiver->decompress("a4b2c2");
}

main();
