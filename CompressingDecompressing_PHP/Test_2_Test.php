<?php

require "./Test_2.php";

class Test_2_Test extends PHPUnit_Framework_TestCase {

    protected function setUp() {
        // Create a stub for the Engine class.
        $this->fileArchiver = new FileArchiver();
    }

    public function testCompress() {
        // Test empty string
        $this->assertEquals($this->fileArchiver->compress(""), "");
        // Test one character
        $this->assertEquals($this->fileArchiver->compress("a"), "a");
        // Test two different characters
        $this->assertEquals($this->fileArchiver->compress("ab"), "ab");
        // Test two same characters
        $this->assertEquals($this->fileArchiver->compress("aa"), "aa");
        // Test three same characters
        $this->assertEquals($this->fileArchiver->compress("aaa"), "a3");
        // Test two same one different
        $this->assertEquals($this->fileArchiver->compress("aab"), "aab");
        // Test three same three same
        $this->assertEquals($this->fileArchiver->compress("aaabbb"), "a3b3");
        // Test a long one
        $this->assertEquals($this->fileArchiver->compress("aaabbccdefghijjjjjjjj"), "a3b2c2d1e1f1g1h1i1j8");
        // Test given test case 1
        $this->assertEquals($this->fileArchiver->compress("aaaabbcc"), "a4b2c2");
        // Test given test case 2
        $this->assertEquals($this->fileArchiver->compress("aabb"), "aabb");
    }
    
    public function testDecompress() {
        // Test given test case 3
        $this->assertEquals($this->fileArchiver->decompress("a4b2c2"), "aaaabbcc");
        // Test empty string
        $this->assertEquals($this->fileArchiver->decompress(""), "");
        // Test silly things that idiots would try
        $this->assertEquals($this->fileArchiver->decompress("a1"), "a");
        $this->assertEquals($this->fileArchiver->decompress("a1-3"), "a---");
        $this->assertEquals($this->fileArchiver->decompress("a3b2c2d1e1f1g1h1i1j8"), "aaabbccdefghijjjjjjjj");
        $this->assertEquals($this->fileArchiver->decompress("a"), "error");
    }
}
