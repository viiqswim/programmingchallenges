  Model in PHP the following scenario:
  - there are three vehicles: car, motorcycle and bike.
  - car and motorcycle have an engine, bike doesn't have an engine.
  - all vehicles can travel.
  - vehicles with an engine need to start their 
       engine before traveling, and stop it after traveling.

I implemented this with PHP and wrote Unit Tests,
applying a test-driven development with PHPUnit.