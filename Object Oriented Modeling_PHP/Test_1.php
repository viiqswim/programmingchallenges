<?php
abstract class Vehicle {

    public function travel() {
        return "I am traveling on a " . get_class($this) . "<br />";
    }

}

Class Engine {

    public function __construct() {
        // Engine starts turned off
        $this->setEngineStatus(false);
    }

    public function start() {
        $this->setEngineStatus(true);
        return "Turning engine on!<br />";
    }

    public function stop() {
        $this->setEngineStatus(false);
        return "Turning engine off!<br />";
    }

    private function setEngineStatus($_engine_status) {
        $this->engine_status = $_engine_status;
    }

    public function getEngineStatus() {
        return $this->engine_status;
    }

}

abstract class MotoredVehicle extends Vehicle {

    public function __construct($engine) {
        $this->setEngine($engine);
    }

    public function travel() {
        $message = $this->getEngine()->start() .
                parent::travel() .
                $this->getEngine()->stop();

        return $message;
    }

    private function getEngine() {
        return $this->engine;
    }

    private function setEngine($engine) {
        $this->engine = $engine;
    }
}

Class Car extends MotoredVehicle {}
Class Motorcycle extends MotoredVehicle {}
Class Bike extends Vehicle {}

function main() {
    $car = new Car(new Engine());
    $motorcycle = new Motorcycle(new Engine());
    $bike = new Bike();
    
    // Uncomment to see results on the browser/command line
//    echo $car->travel();
//    echo $motorcycle->travel();
//    echo $bike->travel();
}

main(); // Starting point