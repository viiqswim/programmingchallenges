<?php

require "./Test_1.php";

class Test_1_Test extends PHPUnit_Framework_TestCase {

    protected function setUp() {
        // Create a stub for the Engine class.
        $this->engine_stub = $this->getMock('Engine');
    }

    public function testTravelingOnCar() {
        $car = new Car($this->engine_stub);
        $this->assertEquals($car->travel(), "I am traveling on a Car<br />");
    }

    public function testTravelingOnMotorcycle() {
        // Create a stub for the SomeClass class.
        $engine_stub = $this->getMock('Engine');
        $motorcycle = new Motorcycle($engine_stub);
        $this->assertEquals($motorcycle->travel(), "I am traveling on a Motorcycle<br />");
    }

    public function testMotoredBike() {
        // Create a stub for the SomeClass class.
        $engine_stub = $this->getMock('Engine');
        $bike = new Bike($engine_stub);
        $this->assertEquals($bike->travel(), "I am traveling on a Bike<br />");
    }

    public function testEngine() {
        $engine = new Engine();
        // Make sure engine starts off
        $this->assertEquals($engine->getEngineStatus(), false);
        // Test start engine
        $this->assertEquals($engine->start(), "Turning engine on!<br />");
        $this->assertEquals($engine->getEngineStatus(), true);
        // Test stop engine
        $this->assertEquals($engine->stop(), "Turning engine off!<br />");
        $this->assertEquals($engine->getEngineStatus(), false);
    }

}
