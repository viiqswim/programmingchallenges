JS Countdowns: write a JavaScript application following these guidelines:
- when the page loads, it should create a number Y of "countdowns" 
(Y can either be a random value or a set value)
- each countdown has an initial random value (from 1 to 10 included)
- every second, each countdown's value is decremented
- a countdown is represented by a single <div> element which displays its value like so:
    - if the value is strictly superior to 0: "I have X seconds left"
    - if the value is strictly equal to 0: "I'm done counting, bye bye!"
- when a countdown reaches 0, it should be removed from the page, 1 second 
after announcing that it's done counting.

I solved the problem using JavScript + jQuery for the implementation.
To test my solutions I used QUnit, and used a test-driven development technique.

