$(document).ready(function() {
    QUnit.module("doCountdown", {
        setup: function() {
            a_list = [];
            a_list = [{rand_num: 10, interval: null}];
            for (var i = 0; i < 2; i++) {
                $('.body').append("<div class='counter " + i + "'></div>");
            }
        },
        teardown: function() {
            $('.body').empty();
            a_list = [];
        }
    });
    QUnit.test("Test should be able to go from 10 to 1", function(assert) {
        var $counter = $(".counter.0");
        for (var i = 10; i > 0; i--) {
            controller.doCountDown(0);
        }
        assert.ok($counter.text() === "I have 1 seconds left");
    });
    QUnit.test("Test should be able to go from 1 to -1", function(assert) {
        var $counter = $(".counter.0");
        a_list[0].rand_num = 1;
        controller.doCountDown(0);
        assert.ok(a_list[0].rand_num === 0);
        assert.ok($counter.text() === "I have 1 seconds left");
        controller.doCountDown(0);
        assert.ok(a_list[0].rand_num === -1);
    });
    QUnit.test("Test should be able to go from -1 to empty message", function(assert) {
        var $counter = $(".counter.0");
        a_list[0].rand_num = -1;
        controller.doCountDown(0); // rand_number should not change
        assert.ok(a_list[0].rand_num === -1);
        assert.ok($counter.text() === "");
    });


    QUnit.module("getRandomNumberModule");
    QUnit.test("Test numbers stay between 1 and 10", function(assert) {
        // Test that getRandomNumber() doesn't return something
        // lower than 1 or bigger than 10 in 100 tries
        var error_flag = false;
        for (var i = 0; i < 100; i++) {
            var rand_num = controller.getRandomNumber(1, 10);
            if (rand_num < 1 || rand_num > 10) {
                error_flag = true;
            }
        }
        assert.ok(!error_flag, "Test random number generates between 1 and 10");
    });

    QUnit.module("createRandomNumbersModule");
    QUnit.test("Test list created has a 100 numbers", function(assert) {
        // Mock getRandomNumber
        controller.getRandomNumber = function(start, end) {
            return 2;
        };
        // Start with a size of 0
        assert.ok(a_list.length === 0);
        controller.createRandomNumbers();
        // Then go to a size of 100
        assert.ok(a_list.length === 100);
    });

    QUnit.module("createCountdownsModule", {
        teardown: function() {
            $(".counter").empty();
        }
    });
    QUnit.test("Test creates 100 new counter divs", function(assert) {
        $(".counter");
        // Start with 0 counter divs
        assert.ok($(".counter").length === 0);
        controller.createCountdowns();
        // Then go to a size of 100
        assert.ok($(".counter").length === 100);
    });

    QUnit.module("createIntervalsModule", {
        setup: function() {
            a_list = [];
        }
    });
    QUnit.test("Test creates 100 new intervals", function(assert) {
        // Mock setInterval, 
        // need to make it global so it overides javascript's setInterval
        window.setInterval = function(callback, time) {
            callback();
            return 1;
        };
        // Mock doCountdown
        controller.doCountDown = function() {
        };
        // Start with 0 counter intervals
        assert.ok(a_list.length === 0);
        // Make a_list size 100 with createRandomNumbers()
        controller.createRandomNumbers();
        controller.createIntervals();
        // Then go to a size of 100, make sure each spot in array has an interval
        var error = false;
        for (var i = 0; i < 100; i++) {
            if (a_list[i].interval === undefined) {
                error = true;
                break;
            }
        }
        assert.ok(a_list.length === 100);
        assert.ok(!error);
    });
});