var controller = {
    main: function() {
        this.createCountdowns();
        this.createRandomNumbers();
        this.createIntervals();
        for (var i = 0; i < 100; i++) {
            this.doCountDown(i);
        }
    },
    // Tested
    createCountdowns: function() {
        for (var i = 0; i < 100; i++) {
            $('body').append("<div class='counter " + i + "'>a</div>");
        }
    },
    // Tested
    getRandomNumber: function(start, end) {
        return Math.floor((Math.random() * end) + start);
    },
    // Tested
    createRandomNumbers: function() {
        for (var i = 0; i < 100; i++) {
            a_list.push({rand_num: this.getRandomNumber(1, 10), interval: null});
        }
    },
    // Tested
    createIntervals: function() {
        for (var i = 0; i < 100; i++) {
            var interval = setInterval(this.doCountDown, 1000, [i]);
            a_list[i].interval = interval;
        }
    },
    // Tested
    doCountDown: function(index) {
        var $counter = $(".counter." + index);
        if (a_list[index].rand_num > 0) {
            var string = "I have " + a_list[index].rand_num + " seconds left";
            $counter.text(string);
            a_list[index].rand_num--;
        } else if (a_list[index].rand_num === 0) {
            $counter.text("I'm done counting, bye bye!");
            a_list[index].rand_num--;
        } else if (a_list[index].rand_num < 0) {
            $counter.text('');
            clearInterval(a_list[index].interval);
        }
    }
};
var a_list = [];
